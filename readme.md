# Rasa nlu prototype

This prototype serves as a backend service that returns matching connectors based on search queries entered by users.
For this rasa needs different input examples. These are defined in the data/nlu.yml. The format of this yaml file is described in detail at https://rasa.com/docs/rasa/nlu-training-data.
Conceptually, the attributes that a connector has are mapped here as entities. This allows the prototype to determine which connector has the greatest overlap with the search query based on the data entered. In the nlu.yml are already examples, which should clarify the idea.


## Installation
For more details see https://rasa.com/docs/rasa/installation
```bash
python3 -m venv ./venv
source ./venv/bin/activate
pip3 install -U --user pip && pip3 install rasa
```

## Usage
To train an NLU model only, run:
```bash
rasa train nlu
```
To try out your NLU model on the command line, run the following command:
```bash
rasa shell nlu
```
This will start the rasa shell and ask you to type in a message to test. You can keep typing in as many messages as you like.

For more details see https://rasa.com/docs/rasa/nlu-only#running-an-nlu-server

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)